package it.unibo.oop.lab.mvc;

import java.util.ArrayList;
import java.util.List;

public class ControllerImpl implements Controller {
    private String toPrint;
    final private List<String> printedHistory;
    
    public ControllerImpl() {
        this.toPrint = null;
        this.printedHistory = new ArrayList<>();
    }
    
    public void setString(final String toPrint) {
        if (toPrint == null) {
            throw new NullPointerException();
        }
        this.toPrint = toPrint;
    }

    public String getString() {
        return this.toPrint;
    }

    public List<String> getHistory() {
        return new ArrayList<>(this.printedHistory);
    }

    public void printString() {
        if (this.toPrint == null) {
            throw new IllegalStateException();
        }
        this.printedHistory.add(this.toPrint);
        System.out.println(this.toPrint);
    }
}
