package it.unibo.oop.lab.mvc;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class TestController {
    @Test public void testSetAndGetString() {
        final Controller contr = new ControllerImpl();
        /*
         *  Verifies that the string is initially null.
         */
        Assert.assertNull(contr.getString());
        /*
         *  Verifies that it throws a NullPointerException when trying to insert a null String.
         */
        try {
            contr.setString(null);
        } catch (NullPointerException e) {
            Assert.assertNotNull(e);
        }
        /*
         * Verifies that it correctly inserts a non-null String.
         */
        try {
            contr.setString("testString");
        } catch (Exception e) {
            Assert.fail();
        }
        Assert.assertEquals("testString", contr.getString());
    }
    
    @Test public void testPrintString() {
        final Controller contr = new ControllerImpl();
        /*
         * Verifies that it throws an IllegalStateExcpetion when the toPrint String is unset.
         */
        try {
            contr.printString();
        } catch (IllegalStateException e) {
            Assert.assertNotNull(e);
        }
        /*
         * Verifies that it correctly prints the String
         */
        try {
            contr.setString("testString");
            contr.printString();
        } catch (Exception e) {
            Assert.fail();
        }
    }
    
    @Test public void testHistory() {
        final Controller contr = new ControllerImpl();
        /*
         * Verifies that the List is empty at the beginning
         */
        Assert.assertTrue(contr.getHistory().isEmpty());
        /*
         * Verifies the content of the List after a few prints
         */
        contr.setString("1");
        contr.printString();
        contr.setString("2");
        contr.printString();
        contr.setString("3");
        contr.printString();
        contr.printString();
        final List<String> history = contr.getHistory();
        Assert.assertTrue(history.size() == 4);
        Assert.assertEquals("1", history.get(0));
        Assert.assertEquals("2", history.get(1));
        Assert.assertEquals("3", history.get(2));
        Assert.assertEquals("3", history.get(3));
    }
}
