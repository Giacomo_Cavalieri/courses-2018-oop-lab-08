package it.unibo.oop.lab.mvc;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUI {

    private final JFrame frame = new JFrame();
    private final Controller controller = new ControllerImpl();

    /*
     * Once the Controller is done, implement this class in such a way that:
     * 
     * 1) It has a main method that starts the graphical application
     * 
     * 2) In its constructor, sets up the whole view
     * 
     * 3) The graphical interface consists of a JTextField in the upper part of the frame, 
     * a JTextArea in the center and two buttons below it: "Print", and "Show history". 
     * SUGGESTION: Use a JPanel with BorderLayout
     * 
     * 4) By default, if the graphical interface is closed the program must exit
     * (call setDefaultCloseOperation)
     * 
     * 5) The behavior of the program is that, if "Print" is pressed, the
     * controller is asked to show the string contained in the text field on standard output. 
     * If "show history" is pressed instead, the GUI must show all the prints that
     * have been done to this moment in the text area.
     * 
     */

    /**
     * builds a new {@link SimpleGUI}.
     */
    public SimpleGUI() {

        /*
         * Make the frame half the resolution of the screen. This very method is
         * enough for a single screen setup. In case of multiple monitors, the
         * primary is selected.
         * 
         * In order to deal coherently with multimonitor setups, other
         * facilities exist (see the Java documentation about this issue). It is
         * MUCH better than manually specify the size of a window in pixel: it
         * takes into account the current resolution.
         */
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);

        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this
         * flag makes the OS window manager take care of the default positioning
         * on screen. Results may vary, but it is generally the best choice.
         */
        frame.setLocationByPlatform(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        
        /*
         * Create the main panel and the south panel that is going to contain
         * two buttons.
         */
        final JPanel mainPanel = new JPanel(new BorderLayout());
        final JTextArea textArea = new JTextArea();
        final JScrollPane centralPane = new JScrollPane(textArea);
        final JPanel southPanel = new JPanel(new BorderLayout());
        mainPanel.add(southPanel, BorderLayout.SOUTH);
        mainPanel.add(centralPane, BorderLayout.CENTER);
        frame.getContentPane().add(mainPanel);

        /*
         * Create the elements contained in the panels; i.e. two buttons, a text field
         * and a text area.
         */
        final JTextField textField = new JTextField();
        final JButton print = new JButton("Print");
        final JButton history = new JButton("Show Hisotry");
        mainPanel.add(textField, BorderLayout.NORTH);
        southPanel.add(print, BorderLayout.WEST);
        southPanel.add(history, BorderLayout.EAST);

        /*
         * Change the properties of the elements.
         */
        centralPane.setBorder(null);
        textField.setEditable(true);
        textArea.setMargin(new Insets(5,5,5,5));
        textArea.setEditable(false);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);

        /*
         * Handlers.
         */
        print.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                controller.setString(textField.getText());
                controller.printString();
            }
        });
        history.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textArea.setText("");
                for (final String string : controller.getHistory()) {
                    textArea.append(string + "\n");
                }
            }
        });
        
    }
    
    private void setVisible() {
        this.frame.setVisible(true);
    }
    
    public static void main(String[] args) {
        SimpleGUI gui = new SimpleGUI();
        gui.setVisible();
    }
}
