package it.unibo.oop.lab.mvc;

import java.util.List;

/**
 * A controller that prints strings and has memory of the strings it printed.
 */
public interface Controller {
    /**
     * 
     * @param toPrint Sets {@link toPrint} as the current String to e printed.
     * @throws NullPointerException In case a null string is passed as a parameter.
     */
    void setString(final String toPrint);

    /**
     * 
     * @return The last String set with {@link setString}.
     */
    String getString();

    /**
     * 
     * @return A {@link List} of Strings representing the history of Strings printed with {@link printString}.
     */
    List<String> getHistory();

    /**
     * Prints on the standard output the last String set with {@link setString}
     * @throws IllegalStateException if the current string is unset.
     */
    void printString();

}
