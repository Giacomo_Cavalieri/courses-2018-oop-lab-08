package it.unibo.oop.lab.mvcio;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;

/**
 * A very simple program using a graphical interface.
 * 
 */
public class SimpleGUI {
    private static final String TITLE = "Prova";
    private final Controller controller = new Controller();
    private final JFrame frame = new JFrame();

    /*
     * Once the Controller is done, implement this class in such a way that:
     * 
     * 1) It has a main method that starts the graphical application
     * 
     * 2) In its constructor, sets up the whole view
     * 
     * 3) The graphical interface consists of a JTextArea with a button "Save" right
     * below (see "ex02.png" for the expected result). SUGGESTION: Use a JPanel with
     * BorderLayout
     * 
     * 4) By default, if the graphical interface is closed the program must exit
     * (call setDefaultCloseOperation)
     * 
     * 5) The program asks the controller to save the file if the button "Save" gets
     * pressed.
     * 
     * Use "ex02.png" (in the res directory) to verify the expected aspect.
     */

    /**
     * builds a new {@link SimpleGUI}.
     */
    public SimpleGUI() {
        frame.setTitle(TITLE);
        /*
         * Make the frame half the resolution of the screen. This very method is enough
         * for a single screen setup. In case of multiple monitors, the primary is
         * selected.
         * 
         * In order to deal coherently with multimonitor setups, other facilities exist
         * (see the Java documentation about this issue). It is MUCH better than
         * manually specify the size of a window in pixel: it takes into account the
         * current resolution.
         */
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);
        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this flag
         * makes the OS window manager take care of the default positioning on screen.
         * Results may vary, but it is generally the best choice.
         */
        frame.setLocationByPlatform(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        /*
         * Creates the elements of the interface, i.e. A JTextArea and a save button
         */
        frame.getContentPane().setLayout(new BorderLayout());
        final JTextArea textArea = new JTextArea();
        frame.getContentPane().add(textArea, BorderLayout.CENTER);
        final JButton save = new JButton("Save");
        frame.getContentPane().add(save, BorderLayout.SOUTH);
        /*
         * Save Button handler
         */
        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                try {
                    final int userChoice = JOptionPane.showConfirmDialog(frame, "Do you want to save?", "Save",
                            JOptionPane.YES_NO_OPTION);
                    if (userChoice == JOptionPane.YES_OPTION) {
                        controller.saveString(textArea.getText());
                    }
                } catch (IOException e1) {
                    System.out.println("An error has occurred.");
                    e1.printStackTrace();
                }
            }
        });
    }

    /**
     * Makes the frame visible.
     */
    public void setVisible() {
        /*
         * make the frame visible
         */
        frame.setVisible(true);
    }

    /**
     * 
     * @param args
     *  ?
     */
    public static void main(final String[] args) {
        final SimpleGUI gui = new SimpleGUI();
        gui.setVisible();
    }

    /**
     * 
     * @return Controller
     *          returns the private controller
     */
    protected Controller getController() {
        return this.controller;
    }

    /**
     * 
     * @return Frame
     *          returns the private frame
     */
    protected JFrame getFrame() {
        return this.frame;
    }
}
