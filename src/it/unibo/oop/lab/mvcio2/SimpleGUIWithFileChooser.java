package it.unibo.oop.lab.mvcio2;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import it.unibo.oop.lab.mvcio.SimpleGUI;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUIWithFileChooser extends SimpleGUI {
    /*
     * 
     * 1) Add a JTextField and a button "Browse..." on the upper part of the
     * graphical interface. Suggestion: use a second JPanel with a second
     * BorderLayout, put the panel in the North of the main panel, put the text
     * field in the center of the new panel and put the button in the line_end of
     * the new panel.
     * 
     * 2) The JTextField should be non modifiable. And, should display the current
     * selected file.
     * 
     * 3) On press, the button should open a JFileChooser. The program should use
     * the method showSaveDialog() to display the file chooser, and if the result is
     * equal to JFileChooser.APPROVE_OPTION the program should set as new file in
     * the Controller the file chosen. If CANCEL_OPTION is returned, then the
     * program should do nothing. Otherwise, a message dialog should be shown
     * telling the user that an error has occurred (use
     * JOptionPane.showMessageDialog()).
     * 
     * 4) When in the controller a new File is set, also the graphical interface
     * must reflect such change. Suggestion: do not force the controller to update
     * the UI: in this example the UI knows when should be updated, so try to keep
     * things separated.
     */

    /**
     * 
     */
    private SimpleGUIWithFileChooser() {
        super();
        final JPanel northPanel = new JPanel(new BorderLayout());
        this.getFrame().getContentPane().add(northPanel, BorderLayout.NORTH);
        final JTextField textField = new JTextField();
        textField.setEditable(false);
        final JButton browse = new JButton("Browse");
        northPanel.add(textField, BorderLayout.CENTER);
        northPanel.add(browse, BorderLayout.LINE_END);
        /*
         * Browse Button handler
         */
        browse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final JFileChooser chooser = new JFileChooser();
                final int userChoice = chooser.showSaveDialog(SimpleGUIWithFileChooser.this.getFrame());
                if (userChoice == JFileChooser.APPROVE_OPTION) {
                    SimpleGUIWithFileChooser.this.getController().setCurrentFile(chooser.getSelectedFile());
                    textField.setText(chooser.getSelectedFile().getPath());
                } else if (userChoice != JFileChooser.CANCEL_OPTION) {
                    JOptionPane.showMessageDialog(SimpleGUIWithFileChooser.this.getFrame(), "An error has occurred.");
                }
            }
        });
    }

    /**
     * 
     * @param args
     *  ?
     */
    public static void main(final String[] args) {
        final SimpleGUIWithFileChooser gui = new SimpleGUIWithFileChooser();
        gui.setVisible();
    }
}
